

## Introduction ##
This is a scala/spray application on spray-can.
You can run it without any servlet container.
Once you have installed sbt on your system, you can run it by typing `sbt run`.
It will listen to all interfaces by using "0.0.0.0" on `appPort`
This application saves information on a humanreadble json file.
Configurations can be found under `resources/application.conf` and `resources/project.properties`

## Configuration ##
* `apiKey` is used to communicate with frontEnd. It is a regular string.
* `dbFile` is used for persisting valid requests. Don't forget to give read/write access.
* `appPort` indicates on which port application should be run.

You can also tune Actors by configuring application.conf
By default it uses different dispatchers for service and Persistenct actors
