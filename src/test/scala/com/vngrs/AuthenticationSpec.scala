package com.vngrs.test

import com.vngrs.DefaultConfig
import com.vngrs.SimpleAuthenticator
import com.vngrs.AuthenticationRejection
import org.scalatest.WordSpec
import org.scalatest.concurrent.ScalaFutures

class SimpleAuthtenicatorSpec extends WordSpec with SimpleAuthenticator with DefaultConfig with ScalaFutures {

  "SimpleAuthenticator" should {
    "authenticate with correct apiKey and password" in {
      assert(auth(conf.apiKey).futureValue === Right(conf.apiKey))
    }

    "not authenticate with wrongKey" in {
      assert(auth("wrongKey").futureValue === Left(AuthenticationRejection) )
    }
  }

}
