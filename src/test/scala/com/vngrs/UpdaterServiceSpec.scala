package com.vngrs.test

import akka.actor.Props
import com.vngrs.UpdaterService
import com.vngrs.DefaultConfig
import com.vngrs.PersisterActor
import com.vngrs.User
import java.io.File
import org.scalatest.BeforeAndAfterAll
import org.scalatest.FlatSpec
import org.scalatest.WordSpec
import spray.http.HttpEntity
import spray.testkit.ScalatestRouteTest
import spray.routing.HttpService
import spray.http.StatusCodes._
import spray.http._
import MediaTypes._
import org.scalatest.Assertions._
import spray.json._
import DefaultJsonProtocol._

class UpdaterServiceSpec extends WordSpec with ScalatestRouteTest with UpdaterService with BeforeAndAfterAll with DefaultConfig {

  val dbFile = File.createTempFile("dbFile", "db")
  val dbFilePath = dbFile.getPath() 
  def actorRefFactory = system
  val persister = system.actorOf(Props(classOf[PersisterActor], dbFilePath), "persister")
  override implicit val ex = scala.concurrent.ExecutionContext.Implicits.global
  
  "API " should {

    "should not add user if apiKey is not sent" in {
      val validUser = User("testUser0", "1234", "example.com", "Id", "itype")
      val postData = HttpEntity(`application/json`, validUser.toJson.prettyPrint)
      Post(s"/users", postData) ~>
        route ~> check {
          assert(status === BadRequest)
          assert(responseAs[String] == "API Key Required")
        }
    }

    "not authenticate if apiKey is not correct" in {
      val user = User("testUser", "123", "example.com", "Id", "itype")
      val postData = HttpEntity(`application/json`, user.toJson.prettyPrint)

      Post(s"/users?apiKey=FAIL", postData) ~>
        sealRoute(route) ~> check {
          assert(status === Unauthorized)
        }
    }

    "add user correctly if user is valid" in {
      val validUser = User("testUser", "123456", "test@example.com", "Id", "itype")
      val postData = HttpEntity(`application/json`, validUser.toJson.prettyPrint)

      Post(s"/users?apiKey=${conf.apiKey}", postData) ~>
        sealRoute(route) ~> check {
          assert(responseAs[String] === "Ok")
        }
    }

    "not add user if user data is invalid" in {
      val longString = (1 to 256).map(_ => "a").mkString("", "", "")
      val fullInvaliUser = User(longString, "12", "example.com", "Id", "hede")
      val postData = HttpEntity(`application/json`, fullInvaliUser.toJson.prettyPrint)
      Post(s"/users?apiKey=${conf.apiKey}", postData) ~>
        sealRoute(route) ~> check {
          assert(status === BadRequest)
        }
    }

    "not add user if user is already in db with same email" in {
      val validUser = User("testUser2", "123456", "test@example.com", "Id", "itype")
      val postData = HttpEntity(`application/json`, validUser.toJson.prettyPrint)
      Post(s"/users?apiKey=${conf.apiKey}", postData) ~>
        sealRoute(route) ~> check {
          assert(status === BadRequest)
        }
    }

    "should not fail with malformed post data" in {
      val postData = HttpEntity(`application/json`, """ "name": "exploit" """)
      Post(s"/users?apiKey=${conf.apiKey}", postData) ~>
        sealRoute(route) ~> check {
          assert(status === BadRequest)
        }
    }

  }

  override def afterAll() {
    dbFile.delete()
  }

}

