package com.vngrs

import akka.actor.Actor
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import java.io.Writer
import scala.util.Try
import spray.json._
import DefaultJsonProtocol._
import Protocol._
import scala.io.Source
import spray.json._
import DefaultJsonProtocol._

class PersisterActor(dbFile: String) extends Actor with ModelMarshallers {

  var writer: PrintWriter = _

  var indexSet = Set.empty[String]
  override def preStart(): Unit = {
    val sourceFile = Try(Source.fromFile(dbFile)).toOption
    val dbFileByLines = sourceFile.map(_.getLines)getOrElse(List.empty[String])
    indexSet = dbFileByLines.foldLeft(indexSet){
      (set, line) => set + line.asJson.convertTo[User].email
    }
    sourceFile.map(_.close())

    writer = new PrintWriter (new FileWriter(new File(dbFile), true))
  }

  def receive: Receive = {
    case u: User => {
      if(!indexSet(u.email)) {
        writer.println(u.toJson.compactPrint)
        //Like a Journal =)
        writer.flush()
        sender ! DBSuccess 
        indexSet += u.email
      }
      else {
        sender ! UserExists
      }
    } 
  }
  
  override def postStop(): Unit = {
    writer.flush
    writer.close
  }

}

object Protocol {
  
  trait Result
  object DBFailed extends Result
  object DBSuccess extends Result 
  object UserExists extends Result
}
