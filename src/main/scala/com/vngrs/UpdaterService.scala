package com.vngrs

import akka.actor.Actor
import akka.actor.ActorRef
import akka.util.Timeout
import scala.concurrent.ExecutionContext
import spray.routing._
import spray.http._
import MediaTypes._
import spray.json._
import DefaultJsonProtocol._
import akka.pattern.ask
import scala.concurrent.duration._
import Protocol._

class UpdaterServiceActor(val persister: ActorRef) extends Actor with UpdaterService with DefaultConfig {

  def actorRefFactory = context

  def receive = runRoute(route)
  override implicit val ex = context.dispatcher
}

trait UpdaterService extends HttpService
  with ModelMarshallers
  with ValidationMarshallers
  with SimpleAuthenticator
  with ConfigComponent
  with Validation
  with Rejections {

  val persister: ActorRef
  implicit val timeout = Timeout(5 seconds)
  implicit val ex: ExecutionContext 

  val route =
    handleRejections(rejectionHandlers) {
      parameter('apiKey) { (key) =>
        authenticate(auth(key)) { apiKey =>
          path("users") {
            post {
              entity(as[User]) { user =>
                validate(user) match {
                  case Valid => {
                    val res = (persister ? user).mapTo[Result]
                    onSuccess(res) {
                      case DBSuccess => complete("Ok")
                      case UserExists => reject(ValidationError(InValid(List(EmailExists))))
                    }
                  }
                  case x @ InValid(res) => reject(ValidationError(x))
                }
              }
            }
          }
        }
      }
    }
}

