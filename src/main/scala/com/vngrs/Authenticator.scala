package com.vngrs

import scala.concurrent.Future
import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import spray.routing.Rejection
import spray.routing.authentication._

trait SimpleAuthenticator extends ConfigComponent  {

  def auth(apiKey: String): Future[Authentication[String]] = {
    Future.successful { if (apiKey == conf.apiKey) Right(apiKey) else Left(AuthenticationRejection) }
  }

}

