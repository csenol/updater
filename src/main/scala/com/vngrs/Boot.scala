package com.vngrs

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._

object Boot extends App with DefaultConfig {

  implicit val system = ActorSystem("vngrs-system")

  val persister = system.actorOf(Props(classOf[PersisterActor], conf.dbFile) , "persister")
  val service = system.actorOf(Props(classOf[UpdaterServiceActor],  persister), "service")

  implicit val timeout = Timeout(5.seconds)

  IO(Http) ? Http.Bind(service, interface = "0.0.0.0", port = conf.appPort)
}
