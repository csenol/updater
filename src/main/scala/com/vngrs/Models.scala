package com.vngrs

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import spray.json.JsString
import spray.json.RootJsonWriter
import spray.json._
import DefaultJsonProtocol._


trait ModelMarshallers extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val userFormat = jsonFormat5(User)
}

object ModelMarshallers extends ModelMarshallers

case class User(userName: String, password: String, email: String, photoId: String, imageFormat: String)

